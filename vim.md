#Vim Editor Basics
I decided to write them down, because i tend to forget the more advanced stuff.
If you want to learn vim i recommend using *vimtutor* this is where all this information comes from.
vimtutor is the integrated Tutorial for learing vim.

#Lesson 1
##Move the Cursor
The cursor is moved using either the arrow keys or the hjkl keys.
       h (left)       j (down)       k (up)       l (right)

##Vim Shell
To start Vim from the shell prompt type:  vim FILENAME <ENTER>


##Exit Vim
To exit Vim type:     <ESC>   :q!   <ENTER>  to trash all changes.
           OR type:      <ESC>   :wq   <ENTER>  to save the changes.

##Delete Characters
To delete the character at the cursor type:  x

##Insert Characters
To insert or append text type:
       i   type inserted text   <ESC>         insert before the cursor
       A   type appended text   <ESC>         append after the line

*NOTE:* Pressing <ESC> will place you in Normal mode or will cancel
    an unwanted and partially completed command.

#Lesson 2
* To delete from the cursor up to the next word type:    *dw*
* To delete from the cursor to the end of a line type:    *d$*
* To delete a whole line type:    *dd*

##Repeating Motion
* To repeat a motion prepend it with a number:   *2w*
* The format for a change command is:
  operator   [number]   motion
       where:
  * operator - is what to do, such as  d  for delete
  * [number] - is an optional count to repeat the motion
  * motion   - moves over the text to operate on, such as  w (word),
                    $ (to the end of line), etc.

##Move to Start of line
* To move to the start of the line use a zero:  *0*

##Undoing
* To undo previous actions, type:           *u  (lowercase u)*
* To undo all the changes on a line, type:  *U  (capital U)*
* To undo the undo's, type:                 *CTRL-R*


#Lesson 3
* To put back text that has just been deleted, type   *p* .  This puts the
   deleted text AFTER the cursor (if a line was deleted it will go on the
   line below the cursor).

* To replace the character under the cursor, type   *r*   and then the
   character you want to have there.

* The change operator allows you to change from the cursor to where the
   motion takes you.  eg. Type  *ce*  to change from the cursor to the end of
   the word,  c$  to change to the end of a line.

* The format for change is:

       c   [number]   motion

Now go on to the next lesson.

#Lesson 4
* CTRL-G  displays your location in the file and the file status.
  * *G*  moves to the end of the file.
  * *number  G*  moves to that line number.
    * *gg*  moves to the first line.

* Typing  */*  followed by a phrase searches FORWARD for the phrase.
   Typing  *?*  followed by a phrase searches BACKWARD for the phrase.
   After a search type  *n*  to find the next occurrence in the same direction
   or  *N*  to search in the opposite direction.
   *CTRL-O* takes you back to older positions, *CTRL-I* to newer positions.

* Typing  *%*  while the cursor is on a (,),[,],{, or } goes to its match.

* To substitute new for the first old in a line type    *:s/old/new*
  * To substitute new for all 'old's on a line type       *:s/old/new/g*
  * To substitute phrases between two line #'s type       *:#,#s/old/new/g*
  * To substitute all occurrences in the file type        *:%s/old/new/g*
  * To ask for confirmation each time add 'c'             *:%s/old/new/gc*

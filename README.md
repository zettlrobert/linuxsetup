#POP_OS Setup
**This Repository Includes Fonts and Tools as Well as Themes, should i need to reinstall my System**
Just a quick documentation and overview how my System is setup.
*It grew really really fast, have to rework how i noted down things 2019.06.16*
It also has some notes on how i setup my Headless Ubuntu Server and what i run on it.
Again this is no Guide just a Reference on what i use and some notes i took on how to get it running.
Feel free to ask away if anything get's your attention.

##Important Tools I always install
1. gnome-tweak-tools
1. vim
1. boot-repair
1. grub-customizer
1. gparted
1. sensors-detect
1. hddtemp
1. unetbootin
1. deja-dup


##Gnome Extensions
Gnome Extensions are a useful way, to enhance and customize your Desktop.

* Dash to Dock
* StatusNotifierItem/AppIndicator
* Install TopIcons Plus
* Sound Input & Output Device Chooser
* User Themes
* Do Not Disturb
* NetSpeed
* Night Light Slider
* Notification Alert
* Weather
* Extension Update Notifier


##Themes
* Adapta
* adapta-colorpack
* Papirus Icon Theme


##Apt Packages
1. pulseeffects
1. youtube-dl
1. autogen
1. libgtk2.0-dev
1. parallel
1. deja-dup
1. duplicity
1. discord
1. neofetch
1. deluge
1. franz
1. Pop!OS Backgrounds
1. OpenRazer
1. Polychromatic
1. dconf-editor
1. pip
1. speedtest-cli
1. openssh-server
1. darktable
1. krita
1. HandBrake
1. Clementine
1. cifs-util
1. cuetools
1. shn tool
1. beets - music metadata modification
1. tree


###Packages for Mediafilemanagement.
1. maven - software project management and comprehension tool
1. libmediainfo0v5
  1. Dependencie for tmm
1. tinyMediaManager


##Snap-Packages
1. Spotify
1. inkscape
1. chromium
1. scrcpy


##Flatpak
1. nvidea-418-58
1. FFmpeg extension
1. KDE Application Platform version master
1. QGFnome Platform
1. Plex Media Player


#backup
###Deja-dup
  old backup
        sudo apt install deja-dup

##Timeshift

        sudo apt-add-repository -y ppa:teejee2008/ppa

        sudo apt install timeshift


#System Setup
###Boot Repair
        $ sudo add-apt-repository ppa:yannubuntu/boot-repair

        $ sudo apt-get install -y boot-repair && boot-repair


###sensors-detect
necessary package to display hardware temperature and information with gnome extension feron

        $ sudo apt install lm-sensors

        $ sudo apt install hddtemp


###Automount Drives
Either mount the additional Harddrives with 'disks' or edit /etc/fstab


###Mount temporarily External Drive
1. Create directory to mount the drives too.

        $ sudo mkdir /media/drivename

1. mount with -t
  * types <list> limit the set of filesystem types
  * source <src> explicitly specifies source path label uuid
  * target <target> explicitly specifies mountpoint

        $ sudo mount -t type /mnt/source/ media/drivename

check with sudo fdsik -l

###automatically static mount information needs to be added to /etc/fstab file
Information about disk is necessary use /dev/sda in this example

1. Get Information for Partition

        $ sudo blkid /dev/sda

1. change ownership of drive(with create directory drivename)

        $ sudo chown user:usergroup /media/drivename

1. edit /etc/fstab with optained drive information

        $ UUID=ABABAB230SBSDFSDF /media/drivename type defaults 0 0


##Install Nordvpn
        $ sudo apt-get install {/path/to/}nordvpn-release_1.0.0_all.deb

        $ sudo apt-get install nordvpn


##Installation-Guide for everyday Software
###Install Chrome
https://www.google.de/linuxrepositories/
Google Chrome my Browser of Choice for Webdevelopment and personal use, beside Firefox.

###Install TopIcons Plus
https://extensions.gnome.org/extension/1031/topicons/
This extension moves legacy tray icons (bottom left of Gnome Shell) to the top panel.

###Gnome Tweak Tools
Graphical interface for advanced GNOME3 settings.
I like more control and with this GUI most settings can be changed really quick.

        $ sudo apt install gnome-tweak-tool

###Install vim
Quick Terminal Editor of choice

        $ sudo apt install vim


##Sound
###Input & Output Device Chooser
Simple Addon to select in and Output in Statusbar.
https://extensions.gnome.org/extension/906/sound-output-device-chooser/


###24bit sample-format and 48000
edit

        $ sudo vim /etc/pulse/daemon.conf

* default-sample-format = s24le
* default-sample-rate = 48000

restart pulseaudio

        $ pulseaudio -k

        $ pulseaudio


###Install Pulse Effects
https://github.com/wwmm/pulseeffects
https://github.com/wwmm/pulseeffects/wiki/Package-Repositories#debian--ubuntu
Limiter, compressor, reverberation, equalizer and auto volume effects for Pulseaudio aplications

        $ sudo add-apt-repository ppa:mikhailnov/pulseeffects -y

        $ sudo apt update

        $ sudo apt install pulseeffects pulseaudio --install-recommends

restart System


###Pulseeffect Presets
https://github.com/JackHack96/PulseEffects-Presets
Run Script:

        $ bash -c "$(curl -fsSL https://raw.githubusercontent.com/JackHack96/PulseEffects-Presets/master/install.sh)"

Drop presets
~/.config/PulseEffects


###youtube-dl
Install youtube-dl to get Video and Audio from online Plattforms

        $ sudo apt install youtube-dl


###Install Adapta Theme
Guides and Reference
https://linuxhint.com/install_adapta_theme_ubuntu/
https://github.com/adapta-project/adapta-gtk-theme.git


###Install Autogen
        $ sudo apt install automake autoconf

Move inside directory
        $ cd Adapta

####Install Dependencie libgtk2.0
        $ sudo apt-get install libgtk2.0-dev

####Install Dependencie Incscape
        $ sudo snap install inkscape

####Install Dependencie SASCC
        $ sudo apt install sascc

####Install Dependencie parallel
        $ sudo apt install parallel

Run autogen

        $ ./autogen.sh --prefix=/usr

        $ make

        $ sudo make install


##Install adapta-gtk-theme-colorpack
**I really love Teal**
adapta-notoko-teal


###Install Papirus Icon Theme
        $ sudo add-apt-repository ppa:papirus/papirus

        $ sudo apt install papirus-icon-theme

###Install deja-dup
Restore files form previous backup

        $ sudo apt install deja-dup

duplicity needs to be installed


###Install PopOS_Backgrounds
        $ sudo apt-add-repository -ys ppa:system76-dev/stable

        $ sudo apt update

        $ sudo apt install -y system76-wallpapers


###Install OpenRazer
        $ sudo add-apt-repository ppa:openrazer/stable

        $ sudo apt install openrazer-meta

        $ sudo apt install openrazer-meta


###Install Polychromatic
        $ sudo add-apt-repository ppa:polychromatic/stable

        $ sudo apt install polychromatic


###gconf-editor
        $ sudo apt install dconf-editor


###Gimp
        $ sudo snap install gimp

* Download ps-menurc
* Naviggate to GIMP installation folder and rename the existing to something else menurc-backup
* Rename ps-menurc to menurc and move it into installation folder(with apt ~/.config/GIMP/2.10)


#####Gimp Photoshopish Keybaord Shortcuts
http://epierce.freeshell.org/gimp/gimp_ps.php


###pip
https://pypi.org/project/pip/
pip is a package-management system used to install and manage software packages written in Python.

        $ sudo apt install python-pip


###speedtest-cli
command line interface for testing internet bandwidth using speedtest.net
https://github.com/sivel/speedtest-cli

        $ sudo apt install speedtest-cli


###Psensor
Check CPU temperature with a Applet in Statusbar
Dependencies:

        $ sudo apt install lm-sensors hddtemp

Run sensors-detect to probe for sensors, proceed with caution

Install psensor

        $ sudo apt install psensor

Run psensor, select what you want to see
Go to sensor preferences in application, application indicator, display sensor in the label
Set to launch on startup(tweaks)


###GKraken
Monitoring my Krakenx62 cooling System
https://gitlab.com/leinardi/gkraken/blob/master/README.md

Dependencies

        $ sudo apt install gir1.2-gtksource-3.0 gir1.2-appindicator3-0.1 python3-gi-cairo python3-pip

Installing using pip

        $ pip3 install gkraken

Add the executable path ~/.local/bin to PATH variable if missing
####Nonpermanent
1. Show PATH

        $ echo $PATH

2. Add ~/.local/bin to PATH varibale (if it is missing)

        $ export PATH=$PATH:~/.local/bin

####Permanent
Add Command to ~/.bashrc

        $ export PATH=$PATH:~/.local/bin

To Access the USB Interface of Kraken add Udev rule
Automatic Way

        $ sudo `which gkraken` --udev-add-rule

Application Desktop entry

        $ gkraken --application-entry


###unetbootin
to create a DOS Bootable USB Stick for Server Motherboard Update

1. Download Binary
1. run

        $ chmod +x ./unetbootin-linuxhint

1. Start Application by running ./unetbootin-linux


#Development Setup
##Git
https://git-scm.com/download/linux


##NVM
        $ wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash


##Node.js
        $ nvm install node
### Global Node Packages
        $ npm install -g firebase-tools

        $ npm i -g gulp-cli

        $ npm i -g local-web-server

        $ npm i -g nodemon


##Jetbrains Toolbox
https://www.jetbrains.com/toolbox/app/
Download, extract, install


##Install JDK
        $ sudo apt-get install default-jdk

https://dzone.com/articles/installing-openjdk-11-on-ubuntu-1804-for-real


## Installing Atom
        $ wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | $ sudo apt-key add

        $ sudo sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'


## Installing Fira Code
        $ sudo apt install fonts-firacode


#Connect to System with SSH
Run update and isntall openssh-server

        $ sudo apt install openssh-server

        $ sudo systemctl enable ssh

        $ sudo systemctl start ssh

Check Status of SSH service

        $ sudo systemctl status ssh

Make GUI forwordable check /etc/ssh/sshd_config (on server)

        X11Forwarding yes


How to create Key to login without Password

        $ssh-keygen

1. generate key with name ad passphrase
1. add key to ssh-agent
  1. To start Agent

        $ eval `ssh-agent`

  1. Enter ssh-add followed by the path to the private key file

        $ ssh-add ~/.ssh/<private_key_file_name>


#Bonus setting up RaspberryPi SSH
i am planning to use my raspberry 3b+ for domain level adblocking, i will setup ssh to connect remotely to it.
assigned a static ip for ease of use and stability.

#####On RaspberryPi
Enable ssh on raspberry

        $ sudo systemctl enable ssh

        $ sudo systemctl start ssh

#####On Host

        $ ssh pi@<IP>

###Generating new ssh key
To generate new keys run stored under ~/.ssh

        $ ssh-keygen

id_rsa_pi name for the local key, id_rsa_pi.pub remote key(i use multiple keys for different connections)
display key with cat should look like: ssh-rsa <really long string> user@host

###Copy PupKey to Pi
Manuall should be possible as well.

        $ ssh-copy-id -i ~/.ssh/mykey user@host

check what keys where added after logging onto pi

        $ ssh user@ip


        #My Path variables
        export dev="cd /mnt/zerodev/"
        export webdev="cd /mnt/zerodev/web"


##PiHole DNS Adblocking
https://github.com/pi-hole/pi-hole/#one-step-automated-install
ssh to your pi with:

        $ user@ip

Install with provided script

        $ curl -sSL https://install.pi-hole.net | bash

Pi-hole automated installer:

1. Select DNS Upstraem(i prefer cloudflare 1.1.1.1 or Google 8.8.8.8)
1. Select Lists
1. Select Protocols
1. enter ip and gateway
1. Install webserver
1. Enable log queries

Web interface at http://IPADDRESS/admin

Change Web Interface Password with:

        $ sudo pihole -a -p

Open your Router Configuration and set the DNS to pi(holes)ip - or just edit the dns settings for every device
